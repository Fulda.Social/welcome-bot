# Welcome Bot

This is a tiny, simple, python based bot for the
[fulda.social](https://fulda.social) mastodon instance.

It's doing nothing more than sending a greeting and some text to new
users on that instance.

# Usage
The bot needs an access token of an admin user in the file
~/.welcome.token.secret and will then start looking for admin
notifications of type signup. Any of those users will receive a
greeting. A timestamp of the run is stored, so later on only new
people will be greeted.

First run stores current time as timestamp, so first run (no timestamp
exists yet) will not greet anyone.
