#!/usr/bin/python3

# Little helper to greet new people on fulda.social
#
# Copyright (C) 2022 Joerg Jaspert <joerg@ganneff.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Need some libraries
# Mastodon, as we want to send toots
from mastodon import Mastodon

# Some os related handlings
import os

# A bit of logging
import logging

# And time handling
import time

# Some logs could be nice
logging.basicConfig(filename="welcome.log", level=logging.INFO)

# Homedir of the current user
homedir = os.path.expanduser("~")
# File we store our lastrun timestamp in
lastrunf = os.path.join(homedir, ".welcome.lastrun")
# File the admin token is stored in
tokensecret = os.path.join(homedir, ".welcome.token.secret")

# We do not want to greet people twice, so we store our last run
# timestamp. And then only greet people that have a created timestamp
# later than this.
if os.path.exists(lastrunf):
    with open(lastrunf, "r") as lastf:
        lastrun = int(lastf.read())
else:
    # Not ever run (or admin deleted the file), so we default to NOW.
    # "Old" accounts won't receive a greeting.
    lastrun = int(time.time())
logging.debug("Lastrun was %s", lastrun)

# Also take timestamp NOW, before we do all work. Or we have a (small)
# race condition. While script runs, user can register. If we take new
# timestamp after run, we could miss them. So take it now.
newlastrun = int(time.time())

logging.info("Setup connection")
mastodon = Mastodon(access_token=tokensecret, api_base_url="https://fulda.social/")

logging.info("Fetching notifications")
notes = mastodon.notifications()
# Check all of them
for item in notes:
    logging.debug("Item ID: %s, created at: %s", item.id, item.created_at)
    userepoch = int(item.created_at.timestamp())
    # We are only interested in signups - that are newer than our last run
    if item.type == "admin.sign_up" and userepoch > lastrun:
        # Who do we talk to?
        newacct = item.account.username
        logging.info("Greet new user: %s" % newacct)
        print("Greet new user: %s" % newacct)
        # Out we yell, heyho, new user
        tootde = mastodon.status_post(
            """Hallo @%s, willkommen auf fulda.social

Wichtig: Sprachen für Posts hier sind Deutsch oder Englisch.

Fragen entweder an (@)lars (ohne die Klammern) oder mich.
Evtl. hilft eine der folgenden Seiten:
https://www.mastodonien.de/mastodon/ oder
https://digitalcourage.de/digitale-selbstverteidigung/fediverse/

Falls Du es noch nicht hast, ich empfehle, deine Bio auszufüllen (Profileinstellungen -> Über mich). Das hilft anderen Nutzern im Fediverse zu wissen, mit wem Sie kommunizieren.
Ebenso bitte möglichst die Zwei-Faktor-Auth anstellen, damit keine einfachen Account-Hacks möglich sind. Geht in Einstellungen unter Konto -> Zwei-Faktor-Authentisierung.

Neben dem normalen Webinterface für Mastodon bieten wir auch eine alternative Oberfläche an: https://phanpy.fulda.social/ - diese hat einige Änderungen und Verbesserungen integriert, schau mal rein, vielleicht gefällt sie Dir.

Ansonsten viel Spass hier.
"""
            % (newacct, newacct),
            visibility="direct",  # Do not spam everyone, make it a direct msg
            language="de",
        )
        logging.info("Posted german welcome, ID is %d" % tootde.id)
        tooten = mastodon.status_post(
            """Hello @%s, welcome to fulda.social

Important note: Languages for posts here are German or English.

Questions to (@)lars (without braces) or me.
Or Maybe one of the following helps:
https://www.mastodonien.de/mastodon/ or
https://digitalcourage.de/digitale-selbstverteidigung/fediverse/

If you haven't done so, I recommend to fill out your bio (Profile settings). It helps other fediverse users to know who they communicate with.
Also please turn on 2-factor auth, so that your account isn't so easy to hijack. You can do so within Account -> Two-factor Auth.

As an alternative to the normal Mastodon Webinterface we also offer https://phanpy.fulda.social/ - that one has a number of changes and advantages integrated. Take a look, maybe you like it.

And finally: Have a good time over here.
"""
            % (newacct, newacct),
            visibility="direct",  # Do not spam everyone, make it a direct msg
            language="en",
            in_reply_to_id=tootde,
        )
        logging.info("Posted english welcome, ID is %d" % tooten.id)

logging.info("Done")

# Quick, sabe the timestamp, so we do not greet them twice.
with open(lastrunf, "w") as lastf:
    lastf.write("%d" % newlastrun)
    logging.debug("Lastrun timestamp: %s", newlastrun)
